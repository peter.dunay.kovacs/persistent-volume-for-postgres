#!/bin/bash

kubectl apply -f -configmap.yaml
kubectl apply -f -volume.yaml
kubectl apply -f -pvc.yaml
kubectl apply -f -deployment.yaml
kubectl apply -f -service.yaml
